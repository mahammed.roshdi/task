<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
    Route::get('login'                          , 'AuthController@showLogin');
    Route::post('login'                         , 'AuthController@login')->name('login');
Route::group(['middleware'               => ['admin']]             , function (){
    Route::get('logout'                         ,'AuthController@logout');
    Route::get('clients'                        ,'ClientController@clients');
    Route::post('create-client'                 ,'ClientController@create');
    Route::post('update-client'                 ,'ClientController@update');
    Route::post('delete-client'                 ,'ClientController@create');

});
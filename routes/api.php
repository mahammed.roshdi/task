<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'namespace'  => 'Api',
], function () {

    Route::post('sign-up', 'Usercontroller@register');
    Route::post('activate', 'Usercontroller@activate');
    Route::post('resend-code', 'Usercontroller@resendCode');

    Route::post('sign-in', 'Usercontroller@signIn');

    Route::post('forget-check-code', 'Usercontroller@forgetCheckCode');
    Route::post('reset-password', 'Usercontroller@resetPassword');
    Route::group(['middleware'               => ['jwt']]             , function (){
        Route::get('profile'               ,'Usercontroller@profile');
        Route::post('logout'                ,'Usercontroller@Logout');
    });
});

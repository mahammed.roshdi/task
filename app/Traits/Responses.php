<?php

namespace App\Traits;
use Illuminate\Http\Exceptions\HttpResponseException;

trait  Responses
{
    public function ApiResponse($key, $msg, $data = [], $anotherKey = [], $page = false) {
        if (auth()->check()) {
          if (auth()->user()->block) {
            $key = 'blocked';
          }
        }
    
        $allResponse['key'] = (string) $key;
        $allResponse['msg'] = (string) $msg;
        $allResponse['data'] = $data;

        if (request('page')) {
          $allResponse['pagination'] = $this->paginationModel($data);
        }
        if ([] != $data || $data != null ) {
          $allResponse['data'] = $data;
        }
        if (!empty($anotherKey)) {
          foreach ($anotherKey as $key => $value) {
            $allResponse[$key] = $value;
          }
        }
        return response()->json($allResponse);
      }

    public function paginationModel($col) {
        $data = [
          'total'         => $col->total() ?? '',
          'count'         => $col->count() ?? '',
          'per_page'      => $col->perPage() ?? '',
          'next_page_url' => $col->nextPageUrl() ?? '',
          'perv_page_url' => $col->previousPageUrl() ?? '',
          'current_page'  => $col->currentPage() ?? '',
          'total_pages'   => $col->lastPage() ?? '',
        ];
        return $data;
      }
}


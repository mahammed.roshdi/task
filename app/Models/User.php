<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'name', 'email','phone', 'password','code','user_type','is_active','is_blocked'
    ];

    protected $hidden = [
        'password'
    ];
    protected $casts = [
        'is_blocked'                   => 'boolean',
        'is_active'                    => 'boolean',
    ];
    public function setPasswordAttribute($value) {
        if ($value) {
            $this->attributes['password'] = bcrypt($value);
        }
    }


    public function getJWTIdentifier()
	{
		return $this->getKey();
	}

    public function getJWTCustomClaims()
	{
		return [];
	}

}

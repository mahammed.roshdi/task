<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Api\BaseApiRequest;

class ActivateRequest extends BaseApiRequest
{
    public function rules()
    {
        return [
            'code'         => 'required|max:50',
            'phone'        => 'required|exists:users,phone',
          ];
    }
}

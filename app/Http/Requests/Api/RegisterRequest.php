<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Api\BaseApiRequest;
class RegisterRequest extends BaseApiRequest
{

    public function rules()
    {
        return [
            'name'         => 'required|max:50',
            'email'        => 'required|email|unique:users,email|max:50',
            'phone'        => 'required|unique:users,phone|max:15',
            'password'     => 'required|min:6|max:100',
          ];
    }
}

<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Api\BaseApiRequest;

class SignInRequest extends BaseApiRequest
{

    public function rules()
    {
        return [
            'phone'        => 'required|exists:users,phone',
            'password'        => 'required',
            'device_id'       => 'required',
            'device_type'       => 'required',
          ];
    }
}

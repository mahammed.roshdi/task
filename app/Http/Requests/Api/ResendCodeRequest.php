<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Api\BaseApiRequest;

class ResendCodeRequest extends BaseApiRequest
{
    public function rules()
    {
        return [
            'phone'        => 'required|exists:users,phone',
          ];
    }
}

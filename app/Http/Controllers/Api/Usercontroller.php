<?php

namespace App\Http\Controllers\Api;

use JWTAuth;
use Carbon\Carbon;
use App\Traits\sms;
use App\Models\User;
use App\Models\UserToken;
use App\Traits\Responses;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Api\SignInRequest;
use App\Http\Requests\Api\ActivateRequest;
use App\Http\Requests\Api\RegisterRequest;
use App\Http\Requests\Api\ResendCodeRequest;

class Usercontroller extends Controller
{
    use Responses,sms;

    public function register(RegisterRequest $request) {
        
      $user = User::create($request->validated());
      $this->sendVerificationCode( $user);
      return $this->ApiResponse('success', __('auth.registered'));
    }
    public function sendVerificationCode($user) {
        $user->update([
            'code'        => rand(1111, 9999),
        ]);

        $msg = trans('auth.activeCode');
        $this->sendSms($user->phone, $msg . $user->code);
    }
    public function activate(ActivateRequest $request) {
        $user = User::where('phone', $request['phone'])->first();

        if ($user->code != $request->code) {
            return $this->ApiResponse('fail', __('auth.code_invalid'));
        }
        $user->update(['code' => null, 'is_active'=>true]);
        return $this->ApiResponse('success', __('auth.activated'));
    }

    public function resendCode(ResendCodeRequest $request) {
        $user =User::where('phone', $request['phone'])->first();
        $this->sendVerificationCode( $user);
        return $this->ApiResponse('success', __('auth.code_re_send'));
    }

    public function signIn(SignInRequest $request){
        $token= JWTAuth::attempt(["phone" => $request['phone'], 'password' => $request['password'] ]);
        if(!$token){  
            return $this->ApiResponse('fail' ,'incorrect_key_or_phone' );
        }
        if (auth()->user()->is_blocked == true){
            return$this->ApiResponse('blocked','blocked' );
        }
        if(auth()->user()->is_active == false)
        {
            $this->sendVerificationCode( auth()->user());
            return$this->ApiResponse('needActive','send activated' );
        }

        $this->updateDeviceId(auth()->user(), $request->all());
        return $this->ApiResponse('success',__('auth.login'),UserResource::make(auth()->user())->setToken($token));
    }



    
    public function updateDeviceId($user, $request) {
        return UserToken::updateOrcreate([
            'device_id' => $request['device_id'],
        ], [
            'device_type' => $request['device_type'],
            'user_id'     => $user->id,
        ]);
    }

    public function Logout(Request $request)
    {
        $token = $request->header('Authorization');
        try {
            $this->deleteToken(auth()->id() , $request->device_id);
            JWTAuth::invalidate($token);
            return $this->ApiResponse('success', 'loggedOut');
        } catch (JWTException $e) {
            return $this->ApiResponse('success', 'something_wrong');
        }
    }

    public function deleteToken($user_id , $device_id)
    {
        UserToken ::where([
            'device_id'   => $device_id,
            'user_id'     => $user_id,
        ])->delete();
    }

    public function profile(Request $request) {
       
        $requestToken = ltrim($request->header('authorization'), 'Bearer ');
        $userData     = UserResource::make(auth()->user())->setToken($requestToken);
        return $this->ApiResponse('success','', $userData);
    }

   
}

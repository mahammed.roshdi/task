<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use App\Traits\Responses;

class JwtMiddleware
{
    use Responses;
    public function handle($request, Closure $next)
    {

        try {
             JWTAuth::parseToken()->authenticate();

            if( auth()->check() )   {
                if(auth()->user()->block)
                    $this->ApiResponse('blocked','you are blocked from admin');
            }

        }
        catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                 $this->ApiResponse('exit',__('auth.invalid_token'));
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                 $this->ApiResponse('exit',__('auth.expired_token'));
            }else{
                 $this->ApiResponse('exit',__('auth.invalid_token'));
            }
        }
        return $next($request);
    }


}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    private $token = '';

    public function setToken($value) {
        $this->token = $value;
        return $this;
    }
    public function toArray($request)
    {
        return [
            'id'                               => (int)     $this->id,
            'name'                             => (string)  $this->name,
            'email'                            => (string)  $this->email,
            'phone'                            => (string)  $this->phone,
            'user_type'                        => (string)  $this->user_type,
            'block'                            => (boolean) $this->is_blocked,
            'active'                           => (boolean) $this->is_active,
            'token'                            => (string)  $this->token,

        ];
    }
}

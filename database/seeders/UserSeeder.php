<?php

namespace Database\Seeders;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create('ar_SA');
        $password =bcrypt(123456);
        for ($i = 0; $i < 200; $i++) {
        $data[] = [
            'name'          => $faker->name,
            'email'         => $faker->unique()->email,
            'password'      => $password,
            'is_blocked'    => rand(0, 1),
            'is_active'     => rand(0, 1),
        ];
        }
        $data[] = [
            'name'          => $faker->name,
            'email'         => $faker->unique()->email,
            'password'      => $password,
            'is_blocked'    => 0,
            'is_active'     => 1,
            'user_type'     => 'admin',
        ];
        DB::table('users')->insert($data);
    }
}
